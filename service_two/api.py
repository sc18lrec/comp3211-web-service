# Endpoint: /combination
# Method: POST
# Arguments: JSON of the form:
#   {'ingredient_list': [{'strDrink': str,
#                         'idDrink': str}, ...],
#   'category_list': [{'strDrink': str,
#                      'idDrink': str}, ...],
#   'random_choice': int}
# Returns: JSON of the form:
#   {'cocktail_choices: ['drink1', 'drink2',...]}

from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse
import random

app = Flask(__name__)
api = Api(app)

# Create a parser to handle the arguments
# We take 1 json argument, containing: two lists of cocktails containing objects of format
# {"strDrink":"69 Special","strDrinkThumb":"https:...","idDrink":"13940"}
# And an integer indicating whether we want a random choice (1) or not (0)
parser = reqparse.RequestParser()
parser.add_argument('ingredient_list', type=dict, action="append", location="json")
parser.add_argument('category_list', type=dict, action="append", location="json")
parser.add_argument('random_choice', type=int, location="json")

# Our resource class that returns common cocktails from a list.
class CommonCocktails(Resource):
    def post(self):

        # Read in the JSON to separate out the two lists
        args = parser.parse_args()
        ingreds = args['ingredient_list']
        categs = args['category_list']

        # Convert lists to sets for easier comparisons.
        # Use the IDs, as these are unique identifiers
        ingred_set = {x['idDrink'] for x in ingreds}
        categ_set = {x['idDrink'] for x in categs}

        # Find the drink IDs that are common to the two sets, and then create a list with the associated names.
        common_cocktails = ingred_set.intersection(categ_set)
        cocktail_choices = [x['strDrink'] for x in ingreds if x['idDrink'] in common_cocktails]
        
        # If the random choice was selected, choose a random cocktail from our list and return it.
        if args['random_choice'] == 1:
            choice = random.choice(cocktail_choices)
            return jsonify(cocktail_choices=[choice])

        # Otherwise return the full list of cocktail choices
        return jsonify(cocktail_choices=cocktail_choices)

api.add_resource(CommonCocktails, '/combination')

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5002)