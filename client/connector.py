# All of the functions required for interfacing with web service APIs
import requests
import os


# Static URL variables for the third party database endpoints being used for cocktail information.
all_ingredients_url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list"
get_ingredients_url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?i="
get_categories_url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c="
get_details_url = "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i="


def getAllIngredients():
    """Get a list of all of the ingredients used in the cocktail database
    Returns: Array[str] of ingredient names"""

    try:
        response = requests.get(all_ingredients_url)
    # Handle unexpected errors from request module.
    except requests.exceptions.RequestException as e:
        return [None, 500]
    # Handle standard errors from web service
    if response.status_code!=200:
        return [None, response.status_code]
    
    # We can reduce the result to just the ingredient names
    ingred_list = response.json()["drinks"]
    all_ingreds = [item['strIngredient1'] for item in ingred_list]
    return all_ingreds


def searchIngredient(ingredient):
    """Get a list of all drinks that can be made from a certain ingredient in the cocktail database
    Argument: Str of ingredient name to search
    Returns: Array[Drink] where Drink is a JSON object containing
            str: strDrink, name of drink
            str: idDrink, identifier for the drink in the database
            str: strDrinkThumb, link to the thumbnail image for the drink"""

    try:
        response = requests.get(get_ingredients_url+ingredient)
    except requests.exceptions.RequestException as e:
        return [None, 500]
    if response.status_code!=200:
        return [None, response.status_code]

    drink_list = response.json()['drinks']
    return drink_list


def searchCategory(category):
    """Get a list of all drinks that are in a certain category in the cocktail database
    Argument: Str of category name to search
    Returns: Array[Drink] where Drink is a JSON object containing
              str: strDrink, name of drink
              str: idDrink, identifier for the drink in the database
              str: strDrinkThumb, link to the thumbnail image for the drink"""

    try:
        response = requests.get(get_categories_url+category)
    # Handle unexpected errors from request module.
    except requests.exceptions.RequestException as e:
        return [None, 500]
    # Handle standard errors from web service
    if response.status_code!=200:
        return [None, response.status_code]

    drink_list = response.json()['drinks']
    return drink_list


def getMoreDrinkDetails(drink_list):
    """Take a list of drinks and return a list with the detailed information for each of them
    Arguments: Array[Drink] where Drink is a JSON object containing at least
            str: idDrink, identifier for the drink in the database
    Returns: Array[Drink] where Drink is a JSON object containing
            str: strDrink, name of drink
            str: idDrink, identifier for the drink in the database
            str: strDrinkThumb, link to the thumbnail image for the drink
            str: strCategory, the name of the category the drink falls under (ordinary drink, cocktail, float, etc.)
            There are some additional fields held within the object, but they are irrelevant for the purposes of this program"""

    # We need the details for each of the drinks in the list, so we loop over them for
    # the requests.
    final_list = []
    for drink in drink_list:
        id = drink['idDrink']

        try:
            response = requests.get(get_details_url + id)
        # Handle unexpected errors from request module.
        except requests.exceptions.RequestException as e:
            return [None, 500]
        # Handle standard errors from web service
        if response.status_code!=200:
            return [None, response.status_code]

        final_list.append(response.json()['drinks'][0])
    return final_list


def getCategoryList(drink_list):
    """Take a list of drinks and return a list of the distinct categories of drink
    Arguments: Array[Drink] where Drink is a JSON object containing
            str: strDrink, name of drink
            str: idDrink, identifier for the drink in the database
            str: strDrinkThumb, link to the thumbnail image for the drink
            str: strCategory, the name of the category the drink falls under (ordinary drink, cocktail, float, etc.)
            There are some additional fields held within the object, but they are irrelevant for the purposes of this program
    Returns: Array[Str] of all the unique categories in the strCategory field of the input array"""

    service_add = os.environ.get("TAIL_CATEGORY_ADDRESS")
    if service_add is None:
        return "Invalid URL"
    
    try:
        response = requests.post(service_add, json={'drinks': drink_list})
    # Handle unexpected errors from request module.
    except requests.exceptions.RequestException as e:
        return [None, 500]
    # Handle standard errors from web service
    if response.status_code!=200:
        return [None, response.status_code]

    return response.json()


def getFinalList(ingred_list, categ_list, random):
    """Take two lists of drinks and return a list of the drinks common to both
    Arguments: Array1[Drink] where Drink is a JSON object containing
            str: strDrink, name of drink
            str: idDrink, identifier for the drink in the database
            str: strDrinkThumb, link to the thumbnail image for the drink
            str: strCategory, the name of the category the drink falls under (ordinary drink, cocktail, float, etc.)
            Array2[Drink] where Drink is a JSON object containing
            str: strDrink, name of drink
            str: idDrink, identifier for the drink in the database
            str: strDrinkThumb, link to the thumbnail image for the drink
            Integer where 1 is a random choice of common cocktail, and 0 is a list of all common cocktails
    Returns: Array[str] of cocktail names """

    service_add = os.environ.get("TAIL_COMBINED_ADDRESS")
    if service_add is None:
        return "Invalid URL"

    try:
        response = requests.post(service_add, json={"ingredient_list":ingred_list, "category_list":categ_list, "random_choice":random})
    #Handle unexpected errors from request module.
    except requests.exceptions.RequestException as e:
        return {'cocktail_choices': None, 'error_code': 500}
    # Handle standard errors from web service
    if response.status_code!=200:
        return {'cocktail_choices': None, 'error_code': response.status_code}

    return response.json()