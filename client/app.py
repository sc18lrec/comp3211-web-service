from flask import Flask, render_template, request, jsonify, url_for, redirect
from . import connector


app = Flask(__name__, template_folder='./templates/')

@app.route('/favicon.ico')
def notFound():
    return "No", 404


# We define two paths here because some of the categories have '/' in them, and we 
# need to preserve the actual value in order to search in the external api
@app.route("/<ingredient>/<category>")
@app.route("/<ingredient>/<path:category>")
def categories(ingredient, category):
    """Generates a page with the list of drink(s) available given the users choice of 
    ingredient and category
    """
    rand = 0
    if "random" in request.args:
        if request.args.get("random") == "true":
            rand = 1

    # Get all results from apis and check they returned correctly.
    ingred_list = connector.searchIngredient(ingredient)
    categ_list = connector.searchCategory(category)

    if ingred_list[0] == None or categ_list[0] == None:
        return render_template('error.html')

    final_result = connector.getFinalList(ingred_list, categ_list, rand)
    if final_result['cocktail_choices'] == None:
        return render_template('error.html')

    final_result = final_result['cocktail_choices']
    return render_template('final.html', choices=final_result)


@app.route("/<ingredient>")
def ingredients(ingredient):
    """Generates a page with a list of categories for the user to select from that are
    available with their previous ingredient choice
    """
    detail_list = connector.getMoreDrinkDetails(connector.searchIngredient(ingredient))
    if detail_list[0] == None:
        return render_template('error.html')

    categories = connector.getCategoryList(detail_list)
    if categories[0] == None:
        return render_template('error.html')

    # If there is only one category we can skip the choices page, as it is irrelevant
    if len(categories) == 1:
        return redirect(url_for("categories", ingredient=ingredient, category=categories[0]))

    return render_template('categories.html', categories=categories)


@app.route("/")
def cocktails():
    """Generates a page with a list of ingredients for the user to select from, based on
    an externally generated list.
    """
    ingredients = connector.getAllIngredients()
    if ingredients[0] == None:
        return render_template('error.html')

    return render_template('home.html', ingredients=ingredients)

if __name__ == '__main__':
    app.run(debug=True)
