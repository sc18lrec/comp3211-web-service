# COMP3211 Web Service

This web service will provide a list of drinks based on an ingredient and category

## Installation

We recommend using a virtual environment with Python 3.6
```bash
python3 -m venv <name>
source <name>/bin/activate
```


To install the required modules use:

```bash
pip install -r requirements.txt
```

## Usage

### Service One

Run the following command to launch the first service:

```bash
python service_one/s1_api.py
```

This will provide an address for the service, which, later, should be exported as below.

```bash
TAIL_CATEGORY_ADDRESS = http://host:port/categories
```

### Service Two

If using a new terminal, you will need to reactivate your venv.

Run the following command to launch the second service:

```bash
python service_two/api.py
```

This will provide an address for the service, which, later, should be exported as below.

```bash
TAIL_COMBINED_ADDRESS = http://host:port/combination
```

### Client

If using a new terminal, you will need to reactivate your venv.

Export both of the environment variables discussed in the steps above in your terminal.

Run the following commands to launch the web server

```bash
cd client
flask run
```

The terminal should then provide an http address to visit in order to access the web interface.

## Using different ports

The default ports for the services are 5001 and 5002. If these are already in use, they can be changed on line 32 of service_one/s1_api.py, or line 57 of service_two/api.py