# Endpoint: /categories
# Method: POST

from flask import Flask
from flask_restful import Resource, Api, reqparse
import requests
import json

app = Flask(__name__)
api = Api(app)

# We create a parser to handle the arguments
# We have one argument - a list of json objects containing the full details of cocktails
parser = reqparse.RequestParser()
parser.add_argument('drinks', type=dict, action="append", help="list of drinks", location="json")

# The get categories class takes the list of cocktails and returns the different categories of drink present
class Categories(Resource):
    def post(self):
        # Read in json file of drinks
        args = parser.parse_args()
        # Compile a set of all the categories found by iterating over each object 
        # The set data structure will disregard duplicated categories
        cat_set = {x['strCategory'] for x in args['drinks']}

        # Return list of categories
        return list(cat_set)
        
api.add_resource(Categories, '/categories')

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5001)